/*
 *****************************************************************************
 * FILE:			rtc.c
 *
 *
 * VERSION: 		1.0
 * AUTHOR: 			Christian Angst, Daniel Schnell<dschnell@posteo.de>
 *
 * DESCRIPTION: 	RTC Stuff for STM32F0 discovery board
 * 					(Clock configuration in driver.c)
 * MODIFICATION:
 *****************************************************************************
*/

//Includes
#include <stddef.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

#include "stm32f0xx.h"
#include <stm32f0xx_rcc.h>
#include <stm32f0xx_rtc.h>
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_exti.h"
#include "stm32f0xx_pwr.h"
#include "stm32f0xx_misc.h"

#include "FreeRTOS.h"
#include "task.h"

#include "logger.h"
#include "rtc.h"
#include "printf.h"
#include "worker_queue.h"

/* defines */
//#define RTC_ALARM_DEBUG   1
#undef RTC_ALARM_DEBUG

#define BKP_VALUE    0x1337

/* forward declarations */

static void rtc_config();
static void rtc_alarm_config(void);

/* function definitions */


/*
 ****************************************************************************
 * FUNCTION: rtc_init
 *
 * DESCRIPTION:
 *
 * INPUT:
 *   	none
 * OUTPUT:
 *   	none
 * RETURN:
 *   	none
 *
 ****************************************************************************
*/
bool rtc_init()
{
    if (RTC_ReadBackupRegister(RTC_BKP_DR0) != BKP_VALUE)
    {
        // rtc configuration
        rtc_config();

        // Config of the RTC data register & RTC prescaler
        RTC_InitTypeDef RTC_InitStructure;
        RTC_InitStructure.RTC_AsynchPrediv = 0x63;
        RTC_InitStructure.RTC_SynchPrediv = 0x18f;
        RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;

        // Check on RTC init
        RTC_DeInit();
        if (RTC_Init(&RTC_InitStructure) == ERROR)
        {
            printf("Error: RTC Prescaler\r\n");
            return false;
        }
    }
    else
    {
        // check the Power On Reset flag
        if (RCC_GetFlagStatus(RCC_FLAG_PORRST) != RESET)
        {
            printf("Error: Power On Reset\r\n");
            return false;
        }
        // check the Pin Reset flag
        else if (RCC_GetFlagStatus(RCC_FLAG_PINRST) != RESET)
        {
            printf("Error: External Reset\r\n");
            return false;
        }

        PWR_BackupAccessCmd(ENABLE);

        // Enable the internal RTC clock
        RCC_LSICmd(ENABLE);

        // Wait for RTC APB registers sync
        RTC_WaitForSynchro();
    }
    rtc_alarm_config();
    return true;
}


/*
 ****************************************************************************
 * FUNCTION: rtc_config
 *
 * DESCRIPTION:
 *
 * INPUT:
 *   	none
 * OUTPUT:
 *   	none
 * RETURN:
 *   	none
 *
 ****************************************************************************
*/
void rtc_config()
{
    PWR_BackupAccessCmd(ENABLE);

    // Enable the internal LSI clock as RTC clock (40 kHz)
    RCC_LSICmd(ENABLE);

    // wait until the clk is ready
    while (RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
    {
    }

    // select the clk src
    RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);

    // Enable the RTC Clock
    RCC_RTCCLKCmd(ENABLE);

    // Wait for RTC APB registers sync
    RTC_WaitForSynchro();
}


/**
 * @brief  Configures the RTC Alarm.
 * @param  None
 * @retval None
 */
static void rtc_alarm_config(void)
{
    /* EXTI configuration */
    EXTI_ClearITPendingBit(EXTI_Line17);
    EXTI_InitTypeDef EXTI_InitStructure;
    EXTI_InitStructure.EXTI_Line = EXTI_Line17;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
    EXTI_InitStructure.EXTI_LineCmd = ENABLE;
    EXTI_Init(&EXTI_InitStructure);

    /* Enable the RTC Alarm Interrupt */
    NVIC_InitTypeDef NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = RTC_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPriority = 0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
#ifdef RTC_ALARM_DEBUG
/* PB0 Debugging per scope */
GPIO_InitTypeDef GPIO_InitStructure;
GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0;
GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
GPIO_Init(GPIOB, &GPIO_InitStructure);
#endif

    /* Set the alarmA Masks */
    RTC_AlarmTypeDef RTC_AlarmStructure;
    RTC_AlarmStructInit(&RTC_AlarmStructure);

    RTC_AlarmStructure.RTC_AlarmMask = RTC_AlarmMask_All;
    RTC_SetAlarm(RTC_Format_BIN, RTC_Alarm_A, &RTC_AlarmStructure);

    /* Set AlarmA sub seconds and enable SubSec Alarm : generate 8 interrupts per Second */
    //RTC_AlarmSubSecondConfig(RTC_Alarm_A, 0x18f, RTC_AlarmSubSecondMask_SS14_5);   // measured: 12,5 Hz
    //RTC_AlarmSubSecondConfig(RTC_Alarm_A, 0x15D, RTC_AlarmSubSecondMask_SS14_9);  // measured: 1 Hz
    RTC_AlarmSubSecondConfig(RTC_Alarm_A, 0x18f, RTC_AlarmSubSecondMask_SS14_9);  // measured: 1 Hz
    //RTC_AlarmSubSecondConfig(RTC_Alarm_A, 0x18f, RTC_AlarmSubSecondMask_SS14_8);  // measured: 2,8 Hz
    //RTC_AlarmSubSecondConfig(RTC_Alarm_A, 0xFF, RTC_AlarmSubSecondMask_SS14_7);     // measured: 3,125 Hz
    //RTC_AlarmSubSecondConfig(RTC_Alarm_A, 0x15D, RTC_AlarmSubSecondMask_SS14_7);  // measured: 1 Hz

    /* Enable AlarmA interrupt */
    RTC_ITConfig(RTC_IT_ALRA, ENABLE);

    /* Enable the alarmA */
    //RTC_AlarmCmd(RTC_Alarm_A, DISABLE);
    RTC_AlarmCmd(RTC_Alarm_A, ENABLE);
}


/**
 * Gets actual time of RealtimeClock.
 *
 * @param   hours       Hours   (0-23)
 * @param   minutes     Minutes (0-59)
 * @param   seconds     Seconds (0-59)
 */
void rtc_time_get(uint8_t* hours, uint8_t* minutes, uint8_t* seconds)
{
    RTC_TimeTypeDef rtc_time;
    RTC_GetTime(RTC_Format_BIN, &rtc_time);
    *hours = rtc_time.RTC_Hours;
    *minutes = rtc_time.RTC_Minutes;
    *seconds = rtc_time.RTC_Seconds;
}


/**
 * Shows actual time saved in Realtime clock
 */
void rtc_date_time_show(void)
{
    uint8_t hours, minutes, seconds, day, month, year;
    rtc_time_get(&hours, &minutes, &seconds);
    rtc_date_get(&day, &month, &year);
    log_msg("Current date/time : %02d.%02d.20%02d, %02d:%02d:%02d\n\r",
            day, month, year, hours, minutes, seconds);
}


/**
 *  Sets actual time of Realtime Clock.
 *
 *  @param  hours       Hours   (0-23)
 *  @param  minutes     Minutes (0-59)
 *  @param  seconds     Seconds (0-59)
 *
 *  @see    rtc_time_get(), rtc_time_show()
 */
void rtc_time_set(uint8_t hours, uint8_t minutes, uint8_t seconds)
{
    RTC_TimeTypeDef rtc_time;

    RTC_TimeStructInit(&rtc_time);
    rtc_time.RTC_Hours = hours;
    rtc_time.RTC_Minutes = minutes;
    rtc_time.RTC_Seconds = seconds;

    ErrorStatus rv = RTC_SetTime(RTC_Format_BIN, &rtc_time);
    if (rv != SUCCESS)
    {
        log_msg("Time could not be set!\r\n");
    }
}

/**
 * Sets date of Realtime clock
 *
 * @param   day     day   (1-31)
 * @param   month   month (1-12)
 * @param   year    year  (0-99)
 */
void rtc_date_set(uint8_t day, uint8_t month, uint8_t year)
{
    RTC_DateTypeDef rtc_date;

    RTC_DateStructInit(&rtc_date);
    rtc_date.RTC_Date = day;
    rtc_date.RTC_Month = month;
    rtc_date.RTC_Year = year;

    ErrorStatus rv = RTC_SetDate(RTC_Format_BIN, &rtc_date);
    if (rv != SUCCESS)
    {
        log_msg("Date could not be set!\r\n");
    }
}


/**
 * Gets date of Realtime clock
 *
 * @param   day     day   (1-31)
 * @param   month   month (1-12)
 * @param   year    year  (0-99)
 */
void rtc_date_get(uint8_t* day, uint8_t* month, uint8_t* year)
{
    RTC_DateTypeDef rtc_date;

    RTC_GetDate(RTC_Format_BIN, &rtc_date);
    *day = rtc_date.RTC_Date;
    *month = rtc_date.RTC_Month;
    *year = rtc_date.RTC_Year;
}


/**
  * @brief  This function handles RTC Alarm interrupt (A and B) request.
  * @param  None
  * @retval None
  */
void RTC_IRQHandler(void)
{
    /* Clear the EXTIL line 17 */
    EXTI_ClearITPendingBit(EXTI_Line17);

#ifdef RTC_ALARM_DEBUG
GPIO_WriteBit(GPIOB, GPIO_Pin_0, Bit_SET);
#endif

    static uint32_t alarm_count = 0;
    Worker_Message_t msg;
    msg.id = WORKER_MSG_RTC_ALARM;
    portBASE_TYPE higher_prio_task_woken = false;

    /* Check on the AlarmA flag and on the number of interrupts per Second (8) */
    if (RTC_GetITStatus(RTC_IT_ALRA) != RESET)
    {
        /* Clear RTC AlarmA Flags */
        RTC_ClearITPendingBit(RTC_IT_ALRA);
        msg.payload.data = alarm_count++;
        xQueueHandle comm_hdl = worker_comm_get_hdl();
        xQueueSendToBackFromISR( comm_hdl, &msg, &higher_prio_task_woken);

        if( higher_prio_task_woken != pdFALSE )
        {
            taskYIELD();
        }
    }
#ifdef RTC_ALARM_DEBUG
GPIO_WriteBit(GPIOB, GPIO_Pin_0, Bit_RESET);
#endif
}

// EOF
